== README

Trabalho de Orientação a Objetos - Prof.: Renato Coral
Alunos: 
		Mariana de Souza Mendes - 14/0154027
		João Pedro Sconetto - 14/0145940



* Ruby version
	ruby 2.1.5p273 (2014-11-13 revision 48405) [i386-mingw32]

* Instruções para funcionamento e execução da página:
	Assegure-se de ter instalado o ruby com a versão acima.

* Abra o terminal na pasta do trabalho:
	~/forum

* Execute o seguinte comando no terminal:
	$ rails s

* Abra o seu navegador no seguinte endereço:
	localhost:3000

* Os diagramas das Models e das Controller se encontram:
	~/forum/doc/*.svg *.png
	
	Obs: Verifique a resolução da tela para não dar erro de exibição na página.
	Em caso de dúvidas ou algum problema favor entrar em contato com os desenvolvidores.
