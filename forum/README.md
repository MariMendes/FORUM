== README

This README would normally document whatever steps are necessary to get the
application up and running.

Trabalho de Orientação a Objetos - Prof.: Renato Coral
Alunos: 
		Mariana de Souza Mendes - 14/0154027
		João Pedro Sconetto - 14/0145940

Things you may want to cover:

* Ruby version
	ruby 2.1.5p273 (2014-11-13 revision 48405) [i386-mingw32]
* System dependencies
	
* Configuration
	
* Database creation
	
* Database initialization
	
* How to run the test suite
	
* Services (job queues, cache servers, search engines, etc.)
	
* Deployment instructions
	
* ...


Please feel free to use a different markup language if you do not plan to run
<tt>rake doc:app</tt>.
