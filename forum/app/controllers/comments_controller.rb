class CommentsController < ApplicationController
	def create
		@post = Post.find(params[:post_id])
		@comment = @post.comments.create(params[:comment].permit(:comment))
		@comment.user_id = current_user.id if current_user
		@comment.save 

		if @comment.save
			redirect_to post_path(@post)
		else
			render 'new'
		end
	end

	def edit
		if current_user.id == Post.find(params[:post_id]).user.id
			@post = Post.find(params[:post_id])
			@comment = @post.comments.find(params[:id])
		else
			redirect_to :back
		end
	end

	def update
		@post = Post.find(params[:post_id])
		@comment = @post.comments.find(params[:id])

		if @comment.update(params[:comment].permit(:comment))
			redirect_to post_path(@post)
		else
			render 'edit'
		end
	end

	def destroy
		if current_user.id == Post.find(params[:post_id]).user.id
			@post = Post.find(params[:post_id])
			@comment = @post.comments.find(params[:id])
			@comment.destroy
			redirect_to post_path(@post)
		else
			redirect_to :back
		end 
	end
end
