class PostsController < ApplicationController
	before_action :find_post, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_user!, except: [:about, :contact]

	def index
		@posts = Post.all.order("created_at DESC")
	end

	def about
	end

	def contact
	end

	def show
	end


	def new
		@post = current_user.posts.build
	end

	def create
		@post = current_user.posts.build(post_params)

		if @post.save
			redirect_to @post
		else
			render 'new'
		end
	end

	def edit
	end

	def update
		if current_user.id == Post.find(params[:id]).user.id
			if @post.update(post_params)
				redirect_to @post
			else
				render 'edit'
			end
		else
			redirect_to @post
		end
	end

	def destroy
		if current_user.id == Post.find(params[:id]).user.id
			@post.destroy
			redirect_to root_path
		else
			redirect_to :back
		end
	end

	private

	def find_post
		@post = Post.find(params[:id])
	end

	def post_params
		params.require(:post).permit(:title, :content)
	end
end
