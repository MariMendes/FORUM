Rails.application.routes.draw do
  devise_for :users
  resources :posts do
  	resources :comments
  end

  root  'posts#index'
  match 'about', controller: 'posts', action: 'about', via: 'get'
  match 'about' => 'posts#about', via: 'get'
  match 'contact', controller: 'posts', action: 'contact', via: 'get'
  match 'contact' => 'posts#contact', via: 'get'
end
